const jsonfile = require("jsonfile");
const randomstring = require("randomstring");
const got = require("got");

const inputURL = "http://www.nactem.ac.uk/software/acromine/dictionary.py?sf=";
const outputFile = "output3.json";

var output = {}
console.log("getting 10 acronyms");
output.definitions = [];
console.log("creating looping function");
/* const fetchData = async () => {
  try{
      var acronym = randomstring.generate(3).toUpperCase();
      
      const mydata = await got(inputURL+acronym);
      console.log("Acronym: ", mydata.body);
      if(mydata.body === "[]\n"){
        
      }else{
      output.definitions.push(mydata.body);
      }
    if (output.definitions.length < 10) {
          console.log("calling looping function again"); 
          fetchData();
        }
  }catch(error){
    console.log(error);
  }
    jsonfile.writeFile(outputFile, output, {spaces: 2}, function(err) {
    console.log("All done!");
    });  
}
fetchData();
 */
console.log("creating looping function")
const getAcronym = function() {
  var acronym = randomstring.generate(3).toUpperCase();
  got(inputURL+acronym).then(response => {
    console.log("got data for acronym", acronym);
    console.log("add returned data to definitions array");
    if(response.body!=="[]\n"){
      output.definitions.push(response.body);
    }
    if (output.definitions.length <= 10) {
      console.log("calling looping function again");
      getAcronym();
    }    
  }).catch(err => {
    console.log(err)
  })
    console.log("saving output file formatted with 2 space indenting");
    jsonfile.writeFile(outputFile, output, {spaces: 2}, function(err) {
      console.log("All done!");
    })  
}
console.log("calling looping function");
getAcronym();