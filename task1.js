const jsonfile = require("jsonfile");
const randomstring = require("randomstring");

const inputFile = "input2.json";
const outputFile = "output2.json";

var output = {}
console.log("loading input json from input2.json");

try{
    jsonfile.readFile(inputFile, function(err, body){

    let names = body.names;
    output.emails = []
    output.emails = names.map((name)=> (name.split("").reverse().join("").concat(randomstring.generate({
    length: 5,
    charset: 'alphabetic'
    }),"@gmail.com").toLowerCase()))

    console.log(output)
    jsonfile.writeFile(outputFile, output, {spaces: 3}, function(err) {
        console.log("All done!");
    });
    });
}catch(error){
    console.log(error)
}


  